package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import Model.DBModel;

public abstract class AddAbstract<T extends DBModel> extends JFrame implements ActionListener {
  private static final long serialVersionUID = 1L;
  private Class<T> type;
  private ArrayList<JLabel> labels;
  private ArrayList<JTextField> textFields;
  private JPanel panel;
  private GroupLayout layout;
  private GroupLayout.SequentialGroup seq;
  private GroupLayout.ParallelGroup par;
  private JButton buttonAdd;

  @SuppressWarnings("unchecked")
  AddAbstract() {
    this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];
    labels = new ArrayList<JLabel>(type.getDeclaredFields().length);
    textFields = new ArrayList<JTextField>(type.getDeclaredFields().length);

    panel = new JPanel();
    layout = new GroupLayout(panel);
    layout.setAutoCreateContainerGaps(true);
    layout.setAutoCreateGaps(true);
    panel.setLayout(layout);

    buttonAdd = new JButton("Add");
    buttonAdd.addActionListener(this);

    seq = layout.createSequentialGroup();
    par = layout.createParallelGroup(Alignment.TRAILING);

    layout.setHorizontalGroup(par);
    layout.setVerticalGroup(seq);

    init();

    add(panel);

    setSize(200, 200);
    setLocationRelativeTo(null);
    setVisible(true);
  }

  private void init() {
    int i = 0;
    for (Field f : type.getDeclaredFields()) {
      if (f.getName().equals("id")) {
        continue;
      }
      JLabel l = new JLabel(f.getName() + ":");
      JTextField t = new JTextField();
      labels.add(l);
      textFields.add(t);
      par.addGroup(layout.createSequentialGroup().addComponent(labels.get(i))
          .addComponent(textFields.get(i)));
      seq.addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(labels.get(i))
          .addComponent(textFields.get(i)));
      ++i;
    }
    par.addComponent(buttonAdd);
    seq.addComponent(buttonAdd);
  }

  public void actionPerformed(ActionEvent e) {
    T obj = null;
    int i = 0;
    try {
      obj = type.getDeclaredConstructor().newInstance();
      for (Field f : type.getDeclaredFields()) {
        if (f.getName().equals("id")) {
          continue;
        }
        f.setAccessible(true);
        Object tmp = f.get(obj);
        if (tmp instanceof Integer) {
          f.set(obj, Integer.parseInt(textFields.get(i).getText()));
        } else if (tmp instanceof Float) {
          f.set(obj, Float.parseFloat(textFields.get(i).getText()));
        } else {
          f.set(obj, textFields.get(i).getText());
        }
        ++i;
      }
      if (addToDatabase(obj)) {
        JOptionPane.showMessageDialog(rootPane, "Succesfully added to database");
      } else {
        JOptionPane.showMessageDialog(rootPane,
            "Could not add " + type.getSimpleName() + " to database");
      }
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
        | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
      e1.printStackTrace();
    } finally {
      this.dispose();
    }
  }

  public abstract boolean addToDatabase(Object o);
}
