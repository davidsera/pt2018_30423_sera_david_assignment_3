package View;

import Logic.ClientBLL;
import Model.Client;

public class AddClient extends AddAbstract<Client> {
  private static final long serialVersionUID = 1L;
  private ClientBLL c;

  public AddClient() {
    c = new ClientBLL();
  }

  @Override
  public boolean addToDatabase(Object o) {
    return c.insert((Client) o);
  }
}
