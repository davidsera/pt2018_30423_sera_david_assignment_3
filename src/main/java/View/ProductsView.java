package View;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import Controller.ProductOrderTableManager;

public class ProductsView extends JFrame {
  private static final long serialVersionUID = 1L;
  private ProductOrderTableManager tableManager;
  private JTable table;
  private JScrollPane scrollPane;

  ProductsView(int id) {
    setSize(400, 400);
    setLocationRelativeTo(null);
    setTitle("Products for order number: " + id);
    tableManager = new ProductOrderTableManager();
    tableManager.loadDataWhereOrder(id);
    table = new JTable(tableManager);
    scrollPane = new JScrollPane(table);
    add(scrollPane);
    setVisible(true);
  }
}
