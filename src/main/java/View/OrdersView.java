package View;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import Controller.OrderTableManager;

public class OrdersView extends JFrame implements ActionListener {
  private static final long serialVersionUID = 1L;
  private OrderTableManager tableManager;
  private JTable table;
  private JScrollPane scrollPane;
  private JButton buttonShowProducts;
  private JPanel panel;
  private GroupLayout layout;

  OrdersView(int id) {
    setSize(400, 400);
    setLocationRelativeTo(null);
    setTitle("Orders for client nr: " + id);
    panel = new JPanel();
    layout = new GroupLayout(panel);
    layout.setAutoCreateContainerGaps(true);
    layout.setAutoCreateGaps(true);
    panel.setLayout(layout);;
    buttonShowProducts = new JButton("Show products for order");
    buttonShowProducts.addActionListener(this);
    buttonShowProducts.setMaximumSize(new Dimension(50, 30));
    tableManager = new OrderTableManager();
    tableManager.loadClient(id);
    table = new JTable(tableManager);
    scrollPane = new JScrollPane(table);
    layout.setHorizontalGroup(layout.createParallelGroup(Alignment.TRAILING)
        .addComponent(scrollPane).addComponent(buttonShowProducts));
    layout.setVerticalGroup(
        layout.createSequentialGroup().addComponent(scrollPane).addComponent(buttonShowProducts));
    add(panel);
    setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    new ProductsView((int) table.getValueAt(table.getSelectedRow(), 0));
  }
}
