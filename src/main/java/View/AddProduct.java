package View;

import Logic.ProductBLL;
import Model.Product;

public class AddProduct extends AddAbstract<Product> {
  private static final long serialVersionUID = 1L;
  private ProductBLL p;

  public AddProduct() {
    p = new ProductBLL();
  }

  @Override
  public boolean addToDatabase(Object o) {
    return p.insert((Product) o);
  }
}
