package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import Controller.ProductOrderTableManager;
import Controller.ProductTableManager;
import Logic.ClientBLL;
import Logic.OrderBLL;
import Model.Client;
import Model.Order;
import Model.ProductOrder;

public class MainView extends JFrame implements ActionListener {
  private static final long serialVersionUID = 1L;
  private JTable tableProducts;
  private JTable tableAddedProducts;
  private ProductTableManager productsManager;
  private ProductOrderTableManager productOrderManager;
  private JPanel panelProducts;
  private JPanel panelClient;
  private JPanel panelMain;
  private JButton buttonAddProduct;
  private JButton buttonRemoveProduct;
  private JButton buttonAddProductToOrder;
  private JButton buttonRemoveProductFromOrder;
  private JButton buttonCreateOrder;
  private JButton buttonShowOrders;
  private JButton buttonShowClients;
  private JTextField textFieldQuantity;
  private JLabel labelClient;
  private JComboBox<Client> comboBoxClients;
  private GroupLayout layoutProducts;
  private GroupLayout layoutClient;
  private GroupLayout layoutMain;

  private OrderBLL orderBLL;
  private ClientBLL clientBLL;

  public MainView() {
    orderBLL = new OrderBLL();
    clientBLL = new ClientBLL();
    init();
  }

  private void init() {
    setTitle("Reflection");
    setLocationRelativeTo(null);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setSize(1000, 500);

    panelProducts = new JPanel();
    layoutProducts = new GroupLayout(panelProducts);
    layoutProducts.setAutoCreateContainerGaps(true);
    layoutProducts.setAutoCreateGaps(true);
    panelProducts.setLayout(layoutProducts);

    panelClient = new JPanel();
    layoutClient = new GroupLayout(panelClient);
    layoutClient.setAutoCreateContainerGaps(true);
    layoutClient.setAutoCreateGaps(true);
    panelClient.setLayout(layoutClient);

    panelMain = new JPanel();
    layoutMain = new GroupLayout(panelMain);
    layoutMain.setAutoCreateContainerGaps(true);
    layoutMain.setAutoCreateGaps(true);
    panelMain.setLayout(layoutMain);

    buttonAddProduct = new JButton("Add product to database");
    buttonAddProduct.addActionListener(this);

    buttonRemoveProduct = new JButton("Remove product from database");
    buttonRemoveProduct.addActionListener(this);

    buttonAddProductToOrder = new JButton("Add product to order");
    buttonAddProductToOrder.addActionListener(this);

    buttonRemoveProductFromOrder = new JButton("Remove product from order");
    buttonRemoveProductFromOrder.addActionListener(this);

    buttonCreateOrder = new JButton("Create new order");
    buttonCreateOrder.addActionListener(this);

    buttonShowOrders = new JButton("Show orders for this client");
    buttonShowOrders.addActionListener(this);

    buttonShowClients = new JButton("All clients");
    buttonShowClients.addActionListener(this);

    textFieldQuantity = new JTextField();

    labelClient = new JLabel("Selected client:");

    JLabel quant = new JLabel("Quantity:");

    comboBoxClients = new JComboBox<Client>();
    ArrayList<Client> clients = (ArrayList<Client>) clientBLL.selectAll();
    for (Client c : clients) {
      comboBoxClients.addItem(c);
    }

    productsManager = new ProductTableManager();
    tableProducts = new JTable(productsManager);
    JScrollPane scrollPane = new JScrollPane(tableProducts);

    productOrderManager = new ProductOrderTableManager();
    tableAddedProducts = new JTable(productOrderManager);
    JScrollPane scrollPaneAdded = new JScrollPane(tableAddedProducts);

    layoutProducts.setHorizontalGroup(layoutProducts.createSequentialGroup()
        .addGroup(layoutProducts.createParallelGroup().addComponent(scrollPane)
            .addGroup(layoutProducts.createSequentialGroup().addComponent(quant)
                .addComponent(textFieldQuantity).addComponent(buttonAddProductToOrder))
            .addGroup(layoutProducts.createSequentialGroup().addComponent(buttonRemoveProduct)
                .addComponent(buttonAddProduct)))
        .addGroup(layoutProducts.createParallelGroup().addComponent(scrollPaneAdded)
            .addGroup(layoutProducts.createSequentialGroup()
                .addComponent(buttonRemoveProductFromOrder).addComponent(buttonCreateOrder))));

    layoutProducts.setVerticalGroup(layoutProducts.createParallelGroup(Alignment.BASELINE)
        .addGroup(layoutProducts.createSequentialGroup().addComponent(scrollPane)
            .addGroup(layoutProducts.createParallelGroup(Alignment.BASELINE).addComponent(quant)
                .addComponent(textFieldQuantity).addComponent(buttonAddProductToOrder))
            .addGroup(layoutProducts.createParallelGroup(Alignment.BASELINE)
                .addComponent(buttonRemoveProduct).addComponent(buttonAddProduct)))
        .addGroup(layoutProducts.createSequentialGroup().addComponent(scrollPaneAdded)
            .addGroup(layoutProducts.createParallelGroup(Alignment.BASELINE)
                .addComponent(buttonRemoveProductFromOrder).addComponent(buttonCreateOrder))));

    layoutClient.setHorizontalGroup(
        layoutClient.createSequentialGroup().addComponent(labelClient).addComponent(comboBoxClients)
            .addComponent(buttonShowOrders).addComponent(buttonShowClients));
    layoutClient.setVerticalGroup(layoutClient.createParallelGroup(Alignment.BASELINE)
        .addComponent(labelClient).addComponent(comboBoxClients).addComponent(buttonShowOrders)
        .addComponent(buttonShowClients));

    layoutMain.setHorizontalGroup(
        layoutMain.createParallelGroup().addComponent(panelClient).addComponent(panelProducts));
    layoutMain.setVerticalGroup(
        layoutMain.createSequentialGroup().addComponent(panelClient).addComponent(panelProducts));

    add(panelMain);

    setVisible(true);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals(buttonAddProduct.getText())) {
      new AddProduct();
      productsManager.fireTableDataChanged();
    } else if (e.getActionCommand().equals(buttonRemoveProduct.getText())) {
      productsManager.deleteRow(tableProducts.getSelectedRow());
      productsManager.fireTableDataChanged();
    } else if (e.getActionCommand().equals(buttonAddProductToOrder.getText())) {
      ProductOrder po = new ProductOrder();
      po.setIdProduct((int) tableProducts.getValueAt(tableProducts.getSelectedRow(), 0));
      po.setQuantity(Integer.parseInt(textFieldQuantity.getText()));
      if (!productOrderManager.insertRow(po)) {
        JOptionPane.showMessageDialog(this, "Not enough stock");
      }
    } else if (e.getActionCommand().equals(buttonRemoveProductFromOrder.getText())) {
      productOrderManager.removeRow(tableAddedProducts.getSelectedRow());
    } else if (e.getActionCommand().equals(buttonCreateOrder.getText())) {
      Order o = new Order();
      ArrayList<ProductOrder> po = productOrderManager.getData();
      o.setDate(LocalDate.now().toString());
      o.setIdClient(((Client) comboBoxClients.getSelectedItem()).getId());

      if (orderBLL.insert(o, po)) {
        orderBLL.createBill(o);
        JOptionPane.showMessageDialog(this, "Successfully created order");
      } else {
        JOptionPane.showMessageDialog(this, "Count not create order");
      }

      productOrderManager.removeAll();
      productsManager.fireTableDataChanged();
    } else if (e.getActionCommand().equals(buttonShowOrders.getText())) {
      new OrdersView(((Client) comboBoxClients.getSelectedItem()).getId());
    } else if (e.getActionCommand().equals(buttonShowClients.getText())) {
      new ClientsView();
    }

    productOrderManager.fireTableDataChanged();
    validate();
  }
}
