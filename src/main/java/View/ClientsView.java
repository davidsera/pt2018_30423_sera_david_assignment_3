package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import Controller.ClientTableManager;

public class ClientsView extends JFrame implements ActionListener {
  private static final long serialVersionUID = 1L;
  private ClientTableManager tableManager;
  private JTable table;
  private JScrollPane scrollPane;
  private JButton buttonAddClient;
  private JButton buttonRemoveClient;
  private JPanel panel;
  private GroupLayout layout;

  ClientsView() {
    setSize(400, 400);
    setLocationRelativeTo(null);
    setTitle("Clients");

    panel = new JPanel();
    layout = new GroupLayout(panel);
    layout.setAutoCreateContainerGaps(true);
    layout.setAutoCreateGaps(true);
    panel.setLayout(layout);

    buttonAddClient = new JButton("Add client");
    buttonAddClient.addActionListener(this);

    buttonRemoveClient = new JButton("Remove client");
    buttonRemoveClient.addActionListener(this);

    tableManager = new ClientTableManager();
    table = new JTable(tableManager);
    scrollPane = new JScrollPane(table);

    layout.setHorizontalGroup(layout.createParallelGroup(Alignment.TRAILING)
        .addComponent(scrollPane).addGroup(layout.createSequentialGroup()
            .addComponent(buttonRemoveClient).addComponent(buttonAddClient)));
    layout.setVerticalGroup(layout.createSequentialGroup().addComponent(scrollPane)
        .addGroup(layout.createParallelGroup(Alignment.BASELINE).addComponent(buttonRemoveClient)
            .addComponent(buttonAddClient)));

    add(panel);
    setVisible(true);
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getActionCommand().equals(buttonRemoveClient.getText())) {
      tableManager.deleteRow(table.getSelectedRow());
    }
    if (e.getActionCommand().equals(buttonAddClient.getText())) {
      new AddClient();
    }
    tableManager.fireTableDataChanged();
  }
}
