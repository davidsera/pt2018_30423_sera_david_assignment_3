package Logic;

import java.util.List;
import DataAccess.ProductDAO;
import Model.Product;

public class ProductBLL {
  private ProductDAO dao;

  public ProductBLL() {
    dao = new ProductDAO();
  }

  private boolean isValid(Product p) {
    boolean valid = true;
    valid = valid && p.getId() >= 0;
    valid = valid && p.getPrice() >= 0;
    valid = valid && p.getStock() >= 0;

    return valid;
  }

  public boolean insert(Product product) {
    if (!isValid(product) || product.getId() != 0) {
      return false;
    }

    product.setId(dao.insert(product));

    return true;
  }

  public Product select(int id) {
    if (id <= 0) {
      return null;
    }

    Product p = dao.select(id);

    return p;
  }

  public List<Product> selectAll() {
    return dao.selectAll();
  }

  public boolean update(Product product) {
    if (!isValid(product)) {
      return false;
    }

    if (dao.select(product.getId()) == null) {
      return false;
    }

    dao.update(product);

    return true;
  }

  public boolean delete(Product product) {
    if (!isValid(product)) {
      return false;
    }

    if (dao.select(product.getId()) == null) {
      return false;
    }

    dao.delete(product);

    return true;
  }
}
