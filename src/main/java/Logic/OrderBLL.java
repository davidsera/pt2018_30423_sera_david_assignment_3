package Logic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import DataAccess.OrderDAO;
import DataAccess.ProductOrderDAO;
import Model.Client;
import Model.Order;
import Model.Product;
import Model.ProductOrder;

public class OrderBLL {
  private OrderDAO dao;
  private ProductOrderDAO productOrderDAO;
  private ClientBLL c;
  private ProductBLL prod;

  public OrderBLL() {
    dao = new OrderDAO();
    productOrderDAO = new ProductOrderDAO();
    c = new ClientBLL();
    prod = new ProductBLL();
  }

  private boolean isValid(Order o) {
    boolean valid = true;
    valid = valid && o.getId() >= 0;
    valid = valid && (c.select(o.getIdClient()) != null);
    // valid = valid && ((o.getDate().compareTo(new Date("01.01.2016")) > 0);
    valid = valid && o.getTotal() >= 0;

    return valid;
  }

  public boolean insert(Order order) {
    if (!isValid(order) || order.getId() != 0) {
      return false;
    }

    order.setId(dao.insert(order));

    return true;
  }

  public boolean insert(Order order, ArrayList<ProductOrder> products) {
    if (!isValid(order)) {
      return false;
    }

    float total = 0;
    for (ProductOrder p : products) {
      Product tmp = prod.select(p.getIdProduct());
      if (tmp == null || p.getQuantity() < 0) {
        return false;
      }
      total += p.getQuantity() * tmp.getPrice();
    }

    order.setTotal(total);

    insert(order);

    for (ProductOrder p : products) {
      p.setIdOrder(order.getId());
      productOrderDAO.insert(p);

      Product tmp = prod.select(p.getIdProduct());
      tmp.setStock(tmp.getStock() - p.getQuantity());
      prod.update(tmp);
    }

    return true;
  }

  public Order select(int id) {
    if (id <= 0) {
      return null;
    }

    Order o = dao.select(id);

    return o;
  }

  public List<Order> selectWhereClientHasId(int id) {
    if (id <= 0) {
      return null;
    }

    List<Order> result = dao.selectWhere("idclient", id);

    return result;
  }

  public ArrayList<Product> selectProductsForOrder(int id) {
    if (id <= 0) {
      return null;
    }

    List<ProductOrder> tmp = productOrderDAO.selectWhere("idorder", id);
    ArrayList<Product> result = new ArrayList<Product>();

    for (ProductOrder p : tmp) {
      Product product = prod.select(p.getIdProduct());
      if (product != null) {
        result.add(product);
      }
    }

    return result;
  }

  public List<ProductOrder> selectProductListForOrder(int id) {
    if (id <= 0) {
      return null;
    }

    List<ProductOrder> result = productOrderDAO.selectWhere("idorder", id);

    return result;
  }

  public List<ProductOrder> selectProductListForOrder(Order order) {
    if (!isValid(order)) {
      return null;
    }

    List<ProductOrder> result = productOrderDAO.selectWhere("idorder", order.getId());

    return result;
  }

  public List<Order> selectAll() {
    return dao.selectAll();
  }

  public boolean update(Order order) {
    if (!isValid(order)) {
      return false;
    }

    if (dao.select(order.getId()) == null) {
      return false;
    }

    dao.update(order);

    return true;
  }

  public boolean delete(Order order) {
    if (!isValid(order)) {
      return false;
    }

    if (dao.select(order.getId()) == null) {
      return false;
    }

    dao.delete(order);

    return true;
  }

  public void createBill(Order o) {
    if (!isValid(o)) {
      return;
    }

    File file = new File("Order_number_" + o.getId() + ".txt");
    try {
      if (!file.createNewFile()) {
        return;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    Client client = c.select(o.getIdClient());
    ArrayList<ProductOrder> products = new ArrayList<ProductOrder>();
    products.addAll(productOrderDAO.selectWhere("idorder", o.getId()));

    try {
      FileWriter writer = new FileWriter(file);

      String eol = System.getProperty("line.separator");

      writer.write("Order number: " + o.getId() + eol + eol);
      writer.append("Client ID: " + client.getId() + eol);
      writer.append("Client name: " + client.getName() + eol);
      writer.append("Client email: " + client.getEmail() + eol);
      writer.append("Client address: " + client.getAddress() + eol + eol);

      writer.append(String.format("%10s %10s %10s %10s %10s %s", "Product ID", "Name", "Price",
          "Quantity", "Total", eol));
      for (ProductOrder p : products) {
        Product pr = prod.select(p.getIdProduct());
        writer.append(String.format("%10s %10s %10s %10s %10s %s", pr.getId(), pr.getName(),
            pr.getPrice(), p.getQuantity(), pr.getPrice() * p.getQuantity(), eol));
      }

      writer.append(eol + "Order total: " + o.getTotal());


      writer.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
