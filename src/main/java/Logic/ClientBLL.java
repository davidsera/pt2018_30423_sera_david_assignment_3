package Logic;

import java.util.List;
import org.apache.commons.validator.routines.EmailValidator;
import DataAccess.ClientDAO;
import Model.Client;

public class ClientBLL {
  private ClientDAO dao;

  public ClientBLL() {
    dao = new ClientDAO();
  }

  private boolean isValid(Client c) {
    boolean valid = true;
    valid = valid && c.getId() >= 0;
    valid = valid && EmailValidator.getInstance().isValid(c.getEmail());
    valid = valid && !c.getName().matches(".*\\d+.*");

    return valid;
  }

  public boolean insert(Client client) {
    if (!isValid(client) || client.getId() != 0) {
      return false;
    }

    client.setId(dao.insert(client));

    return true;
  }

  public Client select(int id) {
    if (id <= 0) {
      return null;
    }

    Client c = dao.select(id);

    return c;
  }

  public List<Client> selectAll() {
    return dao.selectAll();
  }

  public boolean update(Client client) {
    if (!isValid(client)) {
      return false;
    }

    if (dao.select(client.getId()) == null) {
      return false;
    }

    dao.update(client);

    return true;
  }

  public boolean delete(Client client) {
    if (!isValid(client)) {
      return false;
    }

    if (dao.select(client.getId()) == null) {
      return false;
    }

    dao.delete(client);

    return true;
  }
}
