package DataAccess;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.mysql.cj.api.jdbc.Statement;
import java.util.ArrayList;
import java.util.List;
import Model.DBModel;

public class AbstractDAO<T extends DBModel> implements DAO<T> {
  private Class<T> type;

  private Connection connection;
  private PreparedStatement preparedStatement;
  private ResultSet result;

  @SuppressWarnings("unchecked")
  public AbstractDAO() {
    this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];

  }

  public int insert(T obj) {
    int res = -1;
    String query = "INSERT INTO `" + type.getSimpleName() + "` (";
    String tmp = "";

    for (Field f : type.getDeclaredFields()) {
      if (f.getName().equals("id"))
        continue;
      query += f.getName() + ", ";
      tmp += "?,";
    }
    query = query.substring(0, query.length() - 2);
    tmp = tmp.substring(0, tmp.length() - 1);

    query += ") VALUES (" + tmp + ")";

    try {
      connection = ConnectionFactory.connect();
      preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

      int j = 1;
      for (Field f : type.getDeclaredFields()) {
        if (f.getName().equals("id"))
          continue;
        f.setAccessible(true);
        try {
          preparedStatement.setObject(j++, f.get(obj));
        } catch (IllegalArgumentException | IllegalAccessException e) {
          e.printStackTrace();
        } finally {
          f.setAccessible(false);
        }
      }

      preparedStatement.execute();
      result = preparedStatement.getGeneratedKeys();

      if (result.next() && result != null) {
        res = result.getInt(1);
      } else {
        res = -1;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      close();
    }

    return res;
  }

  public List<T> selectAll() {
    String query = "SELECT * FROM `" + type.getSimpleName() + "`";
    List<T> list = null;

    try {
      connection = ConnectionFactory.connect();
      preparedStatement = connection.prepareStatement(query);

      preparedStatement.execute();
      result = preparedStatement.getResultSet();

      list = createObjects(result);
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      close();
    }

    return list;
  }

  public List<T> selectWhere(String property, Object value) {
    String query = "SELECT * FROM `" + type.getSimpleName() + "` WHERE " + property + " LIKE ?";
    List<T> list = null;

    try {
      connection = ConnectionFactory.connect();
      preparedStatement = connection.prepareStatement(query);
      preparedStatement.setObject(1, "%" + value + "%");

      preparedStatement.execute();
      result = preparedStatement.getResultSet();

      list = createObjects(result);
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      close();
    }

    return list;
  }

  public T select(int id) {
    String query = "SELECT * FROM `" + type.getSimpleName() + "` WHERE ID = ?";
    T obj = null;

    try {
      connection = ConnectionFactory.connect();
      preparedStatement = connection.prepareStatement(query);
      preparedStatement.setInt(1, id);

      preparedStatement.execute();
      result = preparedStatement.getResultSet();

      obj = createObjects(result).get(0);
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      close();
    }

    return obj;
  }

  public boolean update(T obj) {
    boolean res = false;
    String query = "UPDATE `" + type.getSimpleName() + "` SET ";

    for (Field f : type.getDeclaredFields()) {
      if (f.getName().equals("id"))
        continue;
      query += f.getName() + "=?, ";
    }
    query = query.substring(0, query.length() - 2);
    query += " WHERE ID = ?";

    try {
      connection = ConnectionFactory.connect();
      preparedStatement = connection.prepareStatement(query);

      int j = 1;
      for (Field f : type.getDeclaredFields()) {
        if (f.getName().equals("id"))
          continue;
        f.setAccessible(true);
        try {
          preparedStatement.setObject(j++, f.get(obj));
        } catch (IllegalArgumentException | IllegalAccessException e) {
          e.printStackTrace();
        } finally {
          f.setAccessible(false);
        }
      }


      Field f = null;
      try {
        f = type.getDeclaredField("id");
        f.setAccessible(true);
        preparedStatement.setObject(j, f.get(obj));
      } catch (NoSuchFieldException | SecurityException | IllegalArgumentException
          | IllegalAccessException e) {
        e.printStackTrace();
      } finally {
        f.setAccessible(false);
      }

      preparedStatement.execute();

      res = true;
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      close();
    }

    return res;
  }

  public boolean delete(int id) {
    boolean res = false;
    String query = "DELETE FROM `" + type.getSimpleName() + "` WHERE ID = ?";

    try {
      connection = ConnectionFactory.connect();
      preparedStatement = connection.prepareStatement(query);
      preparedStatement.setInt(1, id);

      preparedStatement.execute();

      res = true;
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      close();
    }

    return res;
  }

  public boolean delete(T obj) {
    boolean result = false;
    try {
      Field f = type.getDeclaredField("id");
      f.setAccessible(true);
      result = delete((int) f.get(obj));
      f.setAccessible(false);
    } catch (NoSuchFieldException | SecurityException | IllegalAccessException e) {
      e.printStackTrace();
    }

    return result;
  }

  private List<T> createObjects(ResultSet results) {
    List<T> list = new ArrayList<T>();

    try {
      while (results.next() && results != null) {
        try {
          T obj = type.getDeclaredConstructor().newInstance();

          for (Field f : type.getDeclaredFields()) {
            f.setAccessible(true);
            f.set(obj, results.getObject(f.getName()));
            f.setAccessible(false);
          }

          list.add(obj);
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
            | InvocationTargetException | NoSuchMethodException | SecurityException e) {
          e.printStackTrace();
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }

    return list;
  }

  private void close() {
    if (result != null) {
      try {
        result.close();
        result = null;
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    if (preparedStatement != null) {
      try {
        preparedStatement.close();
        preparedStatement = null;
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }

    if (connection != null) {
      try {
        connection.close();
        connection = null;
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}
