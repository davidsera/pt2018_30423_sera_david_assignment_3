package DataAccess;

import java.util.List;
import Model.Product;

public class ProductDAO extends AbstractDAO<Product> {
  public int insert(Product p) {
    return super.insert(p);
  }

  public Product select(int id) {
    return super.select(id);
  }

  public List<Product> selectWhere(String property, Object value) {
    return super.selectWhere(property, value);
  }

  public List<Product> selectAll() {
    return super.selectAll();
  }

  public boolean update(Product p) {
    return super.update(p);
  }

  public boolean delete(int id) {
    return super.delete(id);
  }

  public boolean delete(Product p) {
    return super.delete(p);
  }
}
