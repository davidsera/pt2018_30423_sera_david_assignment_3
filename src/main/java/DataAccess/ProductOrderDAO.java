package DataAccess;

import java.util.List;
import Model.ProductOrder;

public class ProductOrderDAO extends AbstractDAO<ProductOrder> {
  public int insert(ProductOrder po) {
    return super.insert(po);
  }

  public ProductOrder select(int id) {
    return super.select(id);
  }

  public List<ProductOrder> selectWhere(String property, Object value) {
    return super.selectWhere(property, value);
  }

  public List<ProductOrder> selectAll() {
    return super.selectAll();
  }

  public boolean update(ProductOrder po) {
    return super.update(po);
  }

  public boolean delete(int id) {
    return super.delete(id);
  }

  public boolean delete(ProductOrder po) {
    return super.delete(po);
  }
}
