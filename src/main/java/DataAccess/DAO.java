package DataAccess;

import java.util.List;
import Model.DBModel;

public interface DAO<T extends DBModel> {
  public int insert(T obj);
  
  public List<T> selectAll();
  
  public T select(int id);

  public boolean update(T obj);

  public boolean delete(int id);

  public boolean delete(T obj);
}
