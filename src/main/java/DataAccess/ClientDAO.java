package DataAccess;

import java.util.List;
import Model.Client;

public class ClientDAO extends AbstractDAO<Client> {
  public int insert(Client c) {
    return super.insert(c);
  }

  public Client select(int id) {
    return super.select(id);
  }

  public List<Client> selectWhere(String property, Object value) {
    return super.selectWhere(property, value);
  }

  public List<Client> selectAll() {
    return super.selectAll();
  }

  public boolean update(Client c) {
    return super.update(c);
  }

  public boolean delete(int id) {
    return super.delete(id);
  }

  public boolean delete(Client c) {
    return super.delete(c);
  }
}
