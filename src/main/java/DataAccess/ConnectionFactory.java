package DataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
  private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
  private static final String DBURL = "jdbc:mysql://localhost:3306/reflexion?useSSL=false";
  private static final String USERNAME = "root";
  private static final String PASSWORD = "root";

  public static Connection connect() {
    Connection connection = null;
    try {
      Class.forName(DRIVER);
      connection = DriverManager.getConnection(DBURL, USERNAME, PASSWORD);
    } catch (ClassNotFoundException | SQLException e) {
      e.printStackTrace();
    }
    return connection;
  }

  public static void disconnect(Connection connection) {
    if (connection != null) {
      try {
        connection.close();
        connection = null;
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}
