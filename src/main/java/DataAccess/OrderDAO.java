package DataAccess;

import java.util.List;
import Model.Order;

public class OrderDAO extends AbstractDAO<Order> {
  public int insert(Order o) {
    return super.insert(o);
  }

  public Order select(int id) {
    return super.select(id);
  }

  public List<Order> selectWhere(String property, Object value) {
    return super.selectWhere(property, value);
  }

  public List<Order> selectAll() {
    return super.selectAll();
  }

  public boolean update(Order o) {
    return super.update(o);
  }

  public boolean delete(int id) {
    return super.delete(id);
  }

  public boolean delete(Order o) {
    return super.delete(o);
  }
}
