package Model;

public class Client implements DBModel, Cloneable {
  private int id;
  private String name;
  private String address;
  private String email;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Override
  public Object clone() {
    Client c = new Client();
    c.setId(id);
    c.setName(name);
    c.setAddress(address);
    c.setEmail(email);

    return c;
  }

  @Override
  public String toString() {
    return name;
  }
}
