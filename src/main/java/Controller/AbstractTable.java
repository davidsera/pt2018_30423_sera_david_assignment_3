package Controller;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import Model.DBModel;

public abstract class AbstractTable<T extends DBModel> extends AbstractTableModel {
  private static final long serialVersionUID = 1L;
  private Class<T> type;
  protected List<T> data;

  @SuppressWarnings("unchecked")
  public AbstractTable() {
    this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
        .getActualTypeArguments()[0];
  }

  protected abstract void loadData();

  protected abstract boolean update(int row);

  @Override
  public String getColumnName(int i) {
    int j = 0;
    for (Field f : type.getDeclaredFields()) {
      if (j == i) {
        return f.getName();
      }
      j++;
    }
    return null;
  }

  @Override
  public int getColumnCount() {
    return type.getDeclaredFields().length;
  }

  @Override
  public int getRowCount() {
    return data.size();
  }

  @Override
  public boolean isCellEditable(int row, int col) {
    return col > 0;
  }

  @Override
  public Object getValueAt(int row, int col) {
    int j = 0;
    Object res = null;
    for (Field f : type.getDeclaredFields()) {
      if (j == col) {
        try {
          f.setAccessible(true);
          res = f.get(data.get(row));
          f.setAccessible(false);
          return res;
        } catch (IllegalArgumentException | IllegalAccessException e) {
          e.printStackTrace();
        }
      }
      j++;
    }
    return res;
  }

  @Override
  public void setValueAt(Object value, int row, int col) {
    int j = 0;
    for (Field f : type.getDeclaredFields()) {
      if (j == col) {
        try {
          f.setAccessible(true); // set the field accessible
          Object tmp = f.get(data.get(row)); // save the current value
          if (tmp instanceof Integer) { // if the value should be integer
            value = Integer.parseInt((String) value);
          }
          if (tmp instanceof Float) { // if the value should be float
            value = Float.parseFloat((String) value);
          }
          f.set(data.get(row), value); // try and update the value
          if (!update(row)) { // if the validator returns false
            f.set(data.get(row), tmp); // revert the changes
          }
          f.setAccessible(false); // set the field back to private
          return;
        } catch (IllegalArgumentException | IllegalAccessException e) {
          e.printStackTrace();
        }
      }
      j++;
    }
  }

  protected abstract void insertRow(T obj);

  protected abstract void deleteRow(int row);

  @Override
  public void fireTableDataChanged() {
    loadData();
  }
}
