package Controller;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import Logic.OrderBLL;
import Logic.ProductBLL;
import Model.ProductOrder;

public class ProductOrderTableManager extends AbstractTableModel {
  private static final long serialVersionUID = 1L;
  private ArrayList<ProductOrder> data;
  private ProductBLL p;
  private OrderBLL o;

  public ProductOrderTableManager() {
    data = new ArrayList<ProductOrder>();
    p = new ProductBLL();
    o = new OrderBLL();
  }

  public void loadDataWhereOrder(int id) {
    this.data.addAll(o.selectProductListForOrder(id));
  }

  @Override
  public String getColumnName(int col) {
    if (col == 0) {
      return "Product";
    } else {
      return "Quatity";
    }
  }

  @Override
  public int getColumnCount() {
    return 2;
  }

  @Override
  public int getRowCount() {
    return data.size();
  }

  @Override
  public Object getValueAt(int row, int col) {
    if (col == 0) {
      return p.select(data.get(row).getIdProduct()).getName();
    } else {
      return data.get(row).getQuantity();
    }
  }

  public boolean insertRow(ProductOrder productOrder) {
    if (productOrder.getIdProduct() <= 0 || productOrder.getQuantity() < 0) {
      return false;
    }

    int availableQuantity = p.select(productOrder.getIdProduct()).getStock();

    for (ProductOrder po : data) {
      if (po.getIdProduct() == productOrder.getIdProduct()) {
        if (availableQuantity >= po.getQuantity() + productOrder.getQuantity()) {
          po.setQuantity(po.getQuantity() + productOrder.getQuantity());
          return true;
        } else {
          return false;
        }
      }
    }

    if (availableQuantity < productOrder.getQuantity()) {
      return false;
    }

    data.add(productOrder);

    return true;
  }

  public void removeRow(int row) {
    data.remove(row);
  }

  public void removeAll() {
    data.clear();
  }

  public ArrayList<ProductOrder> getData() {
    return data;
  }
}
