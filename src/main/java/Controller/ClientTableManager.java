package Controller;

import Logic.ClientBLL;
import Model.Client;

public class ClientTableManager extends AbstractTable<Client> {
  private static final long serialVersionUID = 1L;
  private ClientBLL c;

  public ClientTableManager() {
    c = new ClientBLL();
    loadData();
  }

  @Override
  public void loadData() {
    Thread t = new Thread(() -> data = c.selectAll());
    t.start();
    try {
      t.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected boolean update(int row) {
    return c.update(data.get(row));
  }

  @Override
  public void insertRow(Client client) {
    if (c.insert(client)) {
      data.add(client);
    }
  }

  @Override
  public void deleteRow(int row) {
    if (c.delete(data.get(row))) {
      data.remove(data.get(row));
    }
  }
}
