package Controller;

import Logic.OrderBLL;
import Model.Order;

public class OrderTableManager extends AbstractTable<Order> {
  private static final long serialVersionUID = 1L;
  private OrderBLL o;

  public OrderTableManager() {
    o = new OrderBLL();
    loadData();
  }

  @Override
  public void loadData() {
    Thread t = new Thread(() -> data = o.selectAll());
    t.start();
    try {
      t.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void loadClient(int id) {
    Thread t = new Thread(() -> data = o.selectWhereClientHasId(id));
    t.start();
    try {
      t.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected boolean update(int row) {
    return o.update(data.get(row));
  }

  @Override
  public void insertRow(Order order) {
    if (o.insert(order)) {
      data.add(order);
    }
  }

  @Override
  public void deleteRow(int row) {
    if (o.delete(data.get(row))) {
      data.remove(data.get(row));
    }
  }
}
