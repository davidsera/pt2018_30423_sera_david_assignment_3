package Controller;

import Logic.ProductBLL;
import Model.Product;

public class ProductTableManager extends AbstractTable<Product> {
  private static final long serialVersionUID = 1L;
  private ProductBLL p;

  public ProductTableManager() {
    p = new ProductBLL();
    loadData();
  }

  @Override
  public void loadData() {
    Thread t = new Thread(() -> data = p.selectAll());
    t.start();
    try {
      t.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  protected boolean update(int row) {
    return p.update(data.get(row));
  }

  @Override
  public void insertRow(Product product) {
    if (p.insert(product)) {
      data.add(product);
    }
  }

  @Override
  public void deleteRow(int row) {
    if (p.delete(data.get(row))) {
      data.remove(data.get(row));
    }
  }
}
